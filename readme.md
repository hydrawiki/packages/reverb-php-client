# Reverb Client

PHP client providing interaction with the Reverb notifications service for
MediaWiki.

## Requirements

* PHP >=7.2
* [Composer][composer]

## Installation

Reverb Client is available on [Packagist][packagist-reverb-client].

```bash
$ composer require hydrawiki/reverb-client php-http/guzzle6-adapter
```

You may swap out [`php-http/guzzle6-adapter`][guzzle-adapter] for your
own [HTTPlug Compatible Client][http-plug-clients],
such as the [cURL client][http-curl-client] or
the [React HTTP Adapter][http-react-adapter].

## Usage

You will need an HTTP Client, a Message Factory and a Reverb endpoint. You may
wish to use [PHP-HTTP's discovery][http-client-discovery] package to
automatically provide the available HTTP Client and Message Factory.

```bash
$ composer require php-http/discovery
```

You are now ready to instantiate a Client using the Client Factory.

```php
use Hydrawiki\Reverb\Client\V1\ClientFactory;

$client = (new ClientFactory)->make(
    HttpClientDiscovery::find(),
    MessageFactoryDiscovery::find(),
    'https://reverb.localhost/api'
);
```



A new Resource is created by passing a `Resource` object to the client's `create`
method for the type, e.g:

```php
$user1 = $client->users()->find('hydra:user:1');
$user2 = $client->users()->find('hydra:user:2');

$notification = new NotificationBroadcast([
    'type'        => 'example',
    'message'     => 'Hello, World!',
    'created-at'  => '2018-01-01 00:00:00',
    'url'         => 'https://www.example.com',
]);

$notification->add('targets', $user1, $user2);

$client->broadcasts()->create($notification);
```

## Development

```bash
$ vendor/bin/phpunit
```

## License

Reverb Client is open-source software licensed under the
[MIT license][mit-license].

[composer]: https://getcomposer.org
[guzzle-adapter]: https://packagist.org/packages/php-http/guzzle6-adapter
[http-plug-clients]: http://docs.php-http.org/en/latest/clients.html
[http-curl-client]: https://packagist.org/packages/php-http/curl-client
[http-react-adapter]: https://packagist.org/packages/php-http/react-adapter
[packagist-reverb-client]: https://packagist.org/packages/hydrawiki/reverb-client
[mit-license]: https://choosealicense.com/licenses/mit/
[http-client-discovery]: https://php-http.readthedocs.io/en/latest/discovery.html
